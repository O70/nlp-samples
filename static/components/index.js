Vue.directive('focus', {
	inserted: function(el) {
		el.focus()
	}
});

new Vue({
	el: '#app-container',
	data() {
		return {
			title: 'NLP Sample',
			keywords: '',
			mininum: 50,
			contents: [],
			home: true,
			loading: false
		}
	},
	created() {
		// this.keywords = '面向非均质储层勘探开发，以空隙结构特征研究为基础，开展流体响应机理研究，主要研究内容有数字岩心孔隙结构分析方法、孔隙结构地震反射特征定量表征方法、频变流体因子反演方法、孔隙结构约束的流体因子构建方法和阻抗域流体因子构建及应用。主要承担单位有中国石油集团科学技术研究院有限功公司和中国石油集团东方地球物理勘探有限责任公司，期望在鄂尔多斯盆地、四川盆地应用。'
		// this.search()
		document.getElementById('page-main').style.marginTop = document.getElementById('page-header').offsetHeight + 'px'
	},
	methods: {
		search() {
			const keywords = this.keywords.trim()
			if (keywords && !this.loading) {
				this.home && (this.home = false)

				const mininum = this.mininum / 100
				console.log('Mininum:', mininum)

				this.loading = true
				setTimeout(() => {
					axios.post('/search', { keywords, mininum }).then(({ data }) => {
						this.contents = data
						this.loading = false
					})
				}, 150)
				
			}
		}
	}
})
