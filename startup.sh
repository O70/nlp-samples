#!/usr/bin/env bash

model=$1
models=(test app)
echo ${models[@]} | grep -wq "${model}" || model=app

clear; python v1/${model}.py
