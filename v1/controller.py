# -*- coding: utf-8 -*-

import web, json, logging
from similarity import Similarity
from summary import Summary

render = web.template.render('templates/')

class Index(object):
	def __init__(self):
		super(Index, self).__init__()

	def GET(self):
		return render.index()

class Search(object):
	def __init__(self):
		super(Search, self).__init__()

	def POST(self):
		web.header('Content-Type', 'application/json')
		params = json.loads(web.data())

		logging.info('Start calculating the similarity......')
		sims = Similarity('./static/files/texts/', './static/stopwords.txt').run(params['keywords'])

		result = []
		logging.info('Start generating summary......')
		for sim in sims:
			result.append({
				'key': sim['key'],
				'title': sim['key'],
				'summary': Summary().run('./static/files/texts/' + sim['key'])
			})

		return json.dumps(result)
