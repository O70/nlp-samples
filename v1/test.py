# -*- coding: utf-8 -*-

from similarity import Similarity
from summary import Summary

import logging
logging.basicConfig(format = '%(asctime)s %(filename)s [%(levelname)s] %(message)s', level = logging.INFO)
logger = logging.getLogger()

def main():
	inputs = [
		'面向非均质储层勘探开发，以空隙结构特征研究为基础，开展流体响应机理研究，主要研究内容有数字岩心孔隙结构分析方法、孔隙结构地震反射特征定量表征方法、频变流体因子反演方法、孔隙结构约束的流体因子构建方法和阻抗域流体因子构建及应用。主要承担单位有中国石油集团科学技术研究院有限功公司和中国石油集团东方地球物理勘探有限责任公司，期望在鄂尔多斯盆地、四川盆地应用。',
		'海外重点区域勘探技术研究，南苏丹、苏丹重点盆地勘探领域评价与目标优选，主要研究内容有中西非裂谷系不同凹陷沉降曲线、构造、沉积响应、成藏组合评价、成藏模式，中西非原油、岩屑样品地球化学分析，Melut盆地北部凹陷重点地区岩性地层圈闭成藏条件，Melut盆地北部凹陷和Muglad盆地S1/2/4区低阻油层识别评价，Melut盆地低勘探程度区和Muglad盆地6区Kaikang槽“三新”领域成藏条件及目标优选，Melut盆地南部火成岩发育区地震资料重新处理，主要承担单位有中国石油集团科学技术研究院有限公司、中国石油集团西北地质研究所有限公司、中国石油集团东方地球物理勘探有限责任公司和中估计石油集团测井有限公司。',
		'面向大庆油田萨北开发区的勘探技术研究，面对萨北开发区特高含水期油田开发，研究内容包括三次采油、水驱调整，过渡带交替注聚，分析二类油层储层的沉积特征，期望增加可采储量。主要承担单位有中国中国石油集团科学技术研究院有限公司和中国石油大庆油田有限责任公司。'
	]

	logger.info('Start calculating the similarity......')
	for i in inputs:
		result = Similarity('./static/files/texts/', './static/stopwords.txt').run(i)

		# logger.info('Start generating summary......')
		# for res in result:
		# 	summ = Summary().run('./static/files/texts/' + res['key'])
		# 	print(res['key'], '***********************************')
		# 	print(summ)

if __name__ == '__main__':
	main()
