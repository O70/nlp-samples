# -*- coding: utf-8 -*-

import web
import logging

logging.basicConfig(format = '%(asctime)s %(filename)s [%(levelname)s] %(message)s', level = logging.INFO)

urls = (
	'/', 'controller.Index',
	'/search', 'controller.Search',
)

if __name__ == '__main__':
	app = web.application(urls, globals())
	app.run()
