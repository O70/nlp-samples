# -*- coding: utf-8 -*-

import os, jieba
from gensim import summarization

class Summary(object):
	def __init__(self):
		super(Summary, self).__init__()
		
	def run(self, filepath):
		with open(filepath) as f: texts = f.read()

		split_texts = []
		for s in texts.split('\n'):
			split_texts.append(' '.join(jieba.lcut(s)))

		res = summarization.summarize('. '.join(split_texts))
		
		return res.replace('.', '').replace(' ', '').replace('\n', '')

	def test_all(self):
		texts = []

		fdir = './static/files/texts/'
		for fn in os.listdir(fdir):
			with open(fdir + fn) as f:
				texts.append(f.read())

		for text in texts:
			print('****************************')
			ss = []
			for s in text.split('\n'):
				ss.append(' '.join(jieba.lcut(s)))

			res = summarization.summarize('. '.join(ss))
			print(res.replace('.', '').replace(' ', '').replace('\n', ''))
