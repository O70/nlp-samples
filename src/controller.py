# -*- coding: utf-8 -*-

import web, json

from gensim import corpora, models, similarities
from src.utils import cut

render = web.template.render('templates/')

model_path = 'static/files/models/'
dictionary = corpora.Dictionary.load(model_path + 'plan.dict')
tfidf_model = models.TfidfModel.load(model_path + 'plan.model')
corpus_vector = corpora.MmCorpus(model_path + 'plan.corpus')

with open('static/stopwords.txt') as f:
	stopwords = [line.strip() for line in f.readlines()]

with open('static/files/jsons/texts.json') as f:
	contents = json.loads(f.read())

class Index(object):
	def __init__(self):
		super(Index, self).__init__()

	def GET(self):
		return render.index()

class Search(object):
	def __init__(self):
		super(Search, self).__init__()

	def POST(self):
		web.header('Content-Type', 'application/json')
		params = json.loads(web.data())

		txt = params.get('keywords').strip()
		mininum = params.get('mininum', 0.5)
		print('Mininum:', mininum)

		print('Search text:', txt)
		if not txt:
			return json.dumps([])

		target_vector = dictionary.doc2bow(cut(txt, stopwords))
		tfidf_target = tfidf_model[target_vector]

		tfidf_corpus = tfidf_model[corpus_vector]
		index = similarities.MatrixSimilarity(tfidf_corpus)
		sim = index[tfidf_target]
		print(sim)
		sim = sim.tolist()

		mappings = []
		for ind in range(len(contents)):
			cont = contents[ind]
			mappings.append({
				'filename': cont.get('filename'),
				# 'name': cont.get('name'),
				'subject': cont.get('subject'),
				'summary': cont.get('summary'),
				'similarity': sim[ind]
			})

		sorts = sorted(mappings, key = lambda x:x['similarity'], reverse = True)

		return json.dumps([r for r in sorts if r.get('similarity') >= mininum])
