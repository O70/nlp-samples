# -*- coding: utf-8 -*-

import os, jieba

def mkdirs(dirpath):
	if not os.path.exists(dirpath):
		os.makedirs(dirpath)

def cut(txt, stopwords = []):
	result = []
	for word in jieba.cut(txt):
		if word and word not in stopwords:
			result.append(word)

	return result
