# -*- coding: utf-8 -*-

import web

urls = (
	'/', 'src.controller.Index',
	'/search', 'src.controller.Search',
)

def run():
	app = web.application(urls, globals())
	app.run()
