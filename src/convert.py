# -*- coding: utf-8 -*-

import os
from win32com import client
from utils import mkdirs

source_dir = os.path.abspath('..') + '/static/files/'

'''
MS Word转为docx/pdf, 需要在Windows下执行
'''
def main():
	word = client.Dispatch('Word.Application')

	doc_dir = source_dir + 'doc/'
	docx_dir = source_dir + 'docx/'
	pdf_dir = source_dir + 'pdf/'
	mkdirs(docx_dir)
	mkdirs(pdf_dir)

	for f in os.listdir(doc_dir):
		docx_name = f.replace('.doc', '.docx')
		pdf_name = f.replace('.doc', '.pdf')

		doc = word.Documents.Open(doc_dir + f)
		# doc = word.Documents.Open(doc_dir + f, ReadOnly = 1)
		doc.SaveAs(docx_dir + docx_name, 16)
		doc.SaveAs(pdf_dir + pdf_name, 17)
		# doc.SaveAs(pdf_dir + pdf_name, FileFormat = 17)
		doc.Close()

	word.Quit()

if __name__ == '__main__':
	main()
